"use strict";

const del = require("del");

module.exports = {};

module.exports.init = function(gulp) {
    gulp.task("clean", function() {
        return del("./build/*");
    });
};
