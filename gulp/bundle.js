"use strict";

const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");

module.exports = {};

module.exports.init = function(gulp) {
    gulp.task("js", () =>{
        let br = browserify({
            entries: "./src/index.js",
        })
        .transform(babelify, { presets: [require.resolve("babel-preset-env")] });

        return br.bundle()
            .pipe(source("bundle.js"))
            .pipe(gulp.dest("build"));
    });
};