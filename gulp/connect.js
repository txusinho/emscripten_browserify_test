"use strict";

const connect = require("gulp-connect");

module.exports = {};

module.exports.init = function(gulp) {
    gulp.task("connect", false, function() {
        return connect.server({
            root: "build"
        });
    });
};
