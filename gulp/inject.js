"use strict";

const inject = require("gulp-inject");

module.exports = {};

module.exports.init = function(gulp) {
    gulp.task("inject-html", () => {
        let target = gulp.src("./html/index.html");
        let sources = gulp.src(["./build/**/*.js"], {read: false});

        return target.pipe(inject(sources, {ignorePath: "build"}))
          .pipe(gulp.dest("./build"));
    });

};