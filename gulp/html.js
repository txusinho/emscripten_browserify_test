"use strict";
module.exports = {};

module.exports.init = function(gulp) {
    gulp.task("copy-html", () => {
        return gulp.src(["html/index.html"])
        .pipe(gulp.dest("build"));
    });
};