global.sinon = require("sinon");

let chai = require("chai");
chai.use(require("sinon-chai"));

global.expect = chai.expect;
global.assert = chai.assert;