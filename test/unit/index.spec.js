require("./helper.js");

import Module from "../../src/main";

describe("Module", () => {
    it("exports function correctly", () => {
        let m = new Module();
        expect(m).to.exist;
        expect(m._int_sqrt(9)).to.deep.equal(3);
    });
});