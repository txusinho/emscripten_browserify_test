"use strict";

const gulp = require("gulp");

let sequence = require("gulp-sequence").use(gulp);

require("./gulp/clean").init(gulp);
require("./gulp/connect").init(gulp);
require("./gulp/inject").init(gulp);
require("./gulp/bundle").init(gulp);

gulp.task("default", (cb)  => { sequence("js", "inject-html", cb); });