# Browserify + empscripten compiled module

This is a test case to integrate emscripten compiled module with browserify.

## Requisites
 - emscripten installed (at least emcc command must be in the path). You have a nice guide [here](http://kripken.github.io/emscripten-site/docs/getting_started/downloads.html)
 - Node installed and available in the path. (This test case has been created using node v4.1.1)
 - Gulp installed system-wide is also welcome :)


 ## Use this test case
1. clone this repo
2. cd emscripten_browserify_test/
3. npm install
4. npm run build
5. gulp connect
6. Open a browser and open http://localhost:8080
7. In the developer console, you should find ~~an error like this~~ "3" :)

Alternatively, you can check the module is correct running `npm test`
